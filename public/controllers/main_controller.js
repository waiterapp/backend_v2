var app = angular.module("WaiterApp");

app.controller('IndexController', function($scope,socket,$http,$localStorage){
		
	$scope.restaurant = [];
	$scope.tables = [];
	$scope.alert_message = "";
	$scope.alert_display = false;

	$http.get('/api/restaurants/index/1').success(function(data){
		$scope.restaurant = data;
		$scope.tables = data.tables;
		$scope.indexTable = -1;

		angular.forEach($scope.tables,function(table,index){

			if($scope.tables[index].orders.length > 0){
				
				$scope.tables[index].order = $localStorage.tables[index].order;//"default";
				$scope.tables[index].count = $scope.tables[index].orders.length;
				/*console.log("Mesa "+$scope.tables[index].name +" "+ $scope.tables[index].count);
				angular.forEach($scope.tables[index].orders, function(o, k){
					if($scope.tables[index].orders[k].status == "Pagado"){
						$scope.tables[index].count = $scope.tables[index].count - 1;
					} 
				});
				console.log("Largo: "+$scope.tables[index].count);*/
			} else {

				$scope.tables[index].order = data.tables.length;
				$scope.tables[index].count = 0;
			}



		});

	})

	$scope.changeBody = function(data) {
		if (data =="Despachada") return "rgba(70,148,8,0.1)"	
		if (data == "Solicitada") return "rgba(2,154,207,0.1)"
		if (data == "Cuenta") return "rgba(245,107,860,0.1)"
	}

	socket.on('rt-change',function(data){

		angular.forEach($scope.tables, function(table,i){
			
			if (data.obj.table_id == table.id) {

				var index = $scope.search(data.obj.id, table.orders)
				$scope.tables[i].order = $scope.indexTable;
				$scope.tables[i].count = $scope.tables[i].orders.length;

				if( index >= 0 & index != 'undefined'){
					
					$scope.tables[i].orders[index] = data.obj;

					switch (data.obj.status){
						case "Cuenta":
							responsiveVoice.speak("Cuenta en "+$scope.tables[i].name, "Spanish Latin American Female");	
							$scope.alert_message = "Cuenta solicitada en "+$scope.tables[i].name
							$scope.alert_display = true
						break;
						case "Solicitada":
							responsiveVoice.speak("Nuevo pedido en "+$scope.tables[i].name,"Spanish Latin American Female")
							$scope.alert_message = "Nueva orden en "+$scope.tables[i].name
							$scope.alert_display = true
						break;
						case "Pagado":
							//console.log("pops");
							//delete $scope.tables[i].orders[index];
							//$scope.tables[i].count--;
							//$localStorage.tables[i].count--;
						break;
					}

					//if(data.obj.status != "Pagado"){
						$localStorage.tables[i].order = $scope.tables[i].order;
						$localStorage.tables[i].count = $scope.tables[i].orders.length;
					/*} else {
						$scope.tables[i].orders.length--;
						$localStorage.tables[i].count = $scope.tables[i].orders.length;
						if($localStorage.tables[i].count <= 0){
							$scope.tables[i].order = $scope.tables.length;
						}
					}*/

				} else {
					$scope.tables[i].orders.unshift(data.obj)

					$localStorage.tables[i].order = $scope.tables[i].order;
					$localStorage.tables[i].count = $scope.tables[i].orders.length;
				}

			} else {
				//$scope.tables[i].order++;
				$scope.tables[i].order = $scope.tables.length;
				$scope.tables[i].count = $scope.tables[i].orders.length;

				$localStorage.tables[i].order = $scope.tables.length;
				$localStorage.tables[i].count = $scope.tables[i].orders.length;
			}

		});
	})

	$scope.search = function(nameKey, myArray){
	    for (var i=0; i < myArray.length; i++) {
	        if (myArray[i].id === nameKey) {
	            return i;
	        }
	    }
	}

	$scope.panelStyle = function(table){
		if(table.orders.length > 0){
			return "info"
		} else {
			return "default"
		}
	}

	$scope.tableSize = function(table){
		if(table.orders.length > 0){
			return "4"
		} else {
			return "2"
		}
	}

	$scope.tableStyle = function(table){
		if(table.orders.length > 0){
			return "height: 200px; overflow:scroll;"
		} else {
			return ""
		}
	}

	$scope.displayOrder = function(order){
		if(order.status === "Pagado"){
			return false;
		}
		return true;
	}
});
