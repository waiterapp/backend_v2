module Api
  module V1
    class RestaurantsController < ApplicationController
      skip_before_filter :verify_authenticity_token  
      #before_filter :set_headers

      respond_to :json

      #Listado de mesas por restaurant: para mesas
      def index
        restaurant = Restaurant.where(:id => params[:id])
        respond_with restaurant[0].to_json(:include =>{ :tables => {:methods => [:orders]}})
      end

      #Listado de ordenes por mesa: para detalle de mesa
      def orders
        orders = Order.where(:table_id => params[:table_id], :status => ["Solicitada","En proceso","Despachada","Cuenta"])
        orders = orders.to_json( 
          :methods => [ :restaurant, :table ], 
          :include => { :orders_products => { :include => :product } }
        )
        respond_with orders
      end

      #Actualizacion del estado de la orden
      def update_order
        data = params[:order]
        
        order = Order.find(data[:id])
        total = 0
        order.update_attributes(:status => data[:status])
        data[:orders_products].each_with_index do |op, index|
          order_product = OrdersProduct.find(op[:id])
          
          case op['status']
            when "Solicitada"
              prod = Product.find(order_product['product_id'])
              total = total + prod.price*op['qty']   
            when "Aprobado"
              prod = Product.find(order_product['product_id'])
              total = total + prod.price*op['qty'] 
          end

          order_product.update_attributes(:status=>op[:status])
        end
        order.update_attributes(:total => total)

        render :json => {:order => order, :result => "ACK"}
      end
    end
  end
end