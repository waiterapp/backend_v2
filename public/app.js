var app = angular.module("WaiterApp", ['ngRoute','ngStorage']);

app.config(function($routeProvider){
  $routeProvider
      .when("/", {
          controller: "IndexController",
          templateUrl: "templates/index.html"
      })
      .when("/details/:table_id", {
          controller: "DetailsController",
          templateUrl: "templates/details.html"
      })
      .otherwise({ redirectTo: '/#' });
});

app.factory('socket', function ($rootScope) {
  var socket = io.connect("http://app.waiter.cl:5001");
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

