class TablesController < ApplicationController
  

  def index
    @tables = Table.where(restaurant_id: params[:restaurant_id])

  end

  def show
    require 'rqrcode_png'
    @table = Table.find(params[:id])

    @qr = RQRCode::QRCode.new(@table.id.to_s).to_img.resize(200, 200).to_data_url
  end

  def new
  	@restaurant = Restaurant.find(params[:restaurant_id])
    
    @table = Table.new
    @tables = @restaurant.tables 
  end

  def create
  	@table = Table.create(table_params)
    @table.qr = @table.id
		if @table.save
			flash[:success] = 'Mesa registrada.'
			redirect_to :back
    else
		  render 'new'
		end
  end

  def destroy
    table = Table.find(params[:id])
    table.destroy
    flash[:success] = 'Mesa eliminada.'
    redirect_to :back
  end

  private
  def table_params
    params.require(:table).permit(:name, :capacity, :restaurant_id)
  end
  
end
