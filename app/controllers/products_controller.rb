class ProductsController < ApplicationController
	
	def destroy
		product = Product.find(params[:id])
		product.destroy
		flash[:success] = 'Producto eliminado.'
		redirect_to :back
	end

end