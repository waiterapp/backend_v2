class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :sku
      t.string :name
      t.string :description
      t.float :price
      t.string :restaurant_id
      t.string :category_id

      t.timestamps null: false
    end
  end
end
