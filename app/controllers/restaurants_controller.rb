class RestaurantsController < ApplicationController

  def index
    @restaurants = Restaurant.search(params[:search], params[:page])
  end

  def new
    @restaurant = Restaurant.new
  end

  def show
    @restaurant = Restaurant.find(params[:id])
    @category = Category.new
    @product = Product.new

    @categories = Category.where(restaurant_id: @restaurant.id).order(:order)
    @products = Product.where(restaurant_id: @restaurant.id).order(:order)
    #@menus = Menu.getMenusByRestaurant(params[:id])
  end

  def edit
    @restaurant = Restaurant.find(params[:id])
  end

  def update
    @restaurant = Restaurant.find(params[:id])
 
    if @restaurant.update(restaurant_params)
      redirect_to @restaurant
    else
      render 'edit'
    end
  end

  def create
    @restaurant = Restaurant.create(restaurant_params)
    #redirect_to restaurant_path(@restaurant)
    respond_to do |format|
      if @restaurant.save
        #redirect_to @restaurant
        flash[:success] = 'Restaurant registrado.'
        format.html { redirect_to @restaurant }
      else
        render 'new'
      end
    end
  end

  def destroy
    @restaurant = Restaurant.find(params[:id])
    @restaurant.destroy
    respond_to do |format|
      flash[:success] = 'Restaurant eliminado.'
      format.html { redirect_to restaurants_url }
    end
  end

  def add_categories
    category = Category.create(category_params)
    @restaurant = Restaurant.find(category.restaurant_id)
    #redirect_to restaurant_path(@restaurant)
    if category.save
      #redirect_to @restaurant
      flash[:success] = 'Categoria guardada.'
      redirect_to @restaurant 
    else
      render 'new'
    end
  end

  def add_products
    product = Product.create(product_params)
    @restaurant = Restaurant.find(product.restaurant_id)

    if product.save
      flash[:success] = 'Producto guardado.'
      redirect_to @restaurant
    else
      render 'new'
    end
  end


  private
  def restaurant_params
    params.require(:restaurant).permit(:name, :code, :address, :type_id)
  end
  def category_params
    params.require(:category).permit(:name, :image, :order, :restaurant_id)
  end
  def product_params
    params.require(:product).permit(:name, :price, :image, :sku, :category_id, :order, :restaurant_id)
  end
end
