var app = angular.module("WaiterApp")

app.controller('DetailsController', function($scope,$http, $routeParams,socket){
	


	socket.on('rt-change',function(data){
		if (data.obj.table_id == $routeParams.table_id) {
			$http.get('/api/restaurants/orders/'+$routeParams.table_id).success(function(orders){
				$scope.orders = orders

				angular.forEach(orders, function(order,index){
					$scope.restaurant_name = order.restaurant.name;
					$scope.table_name = order.table.name;
				});

			})
		}
	});



	$http.get('/api/restaurants/orders/'+$routeParams.table_id).success(function(orders){
		$scope.orders = orders

		angular.forEach(orders, function(order,index){
			$scope.restaurant_name = order.restaurant.name;
			$scope.table_name = order.table.name;
		});

	})

	$scope.update_order = function(order){
		
		var result = $http.post('/api/restaurants/update_order', { 'order' : order});
		result.success(function(data, status, headers, config) {
			//$scope.message = data;
			console.log(data);
			//$scope.pushNotification(device);
			send_push = false
			push_notification = ""
			switch(data.order.status){
				case "Despachada":
					push_notification = "En un instante su pedido llegará a su mesa. Disfrútelo!"
					send_push = true
					break
				case "En proceso":
					push_notification = "Su pedido está siendo elaborado."
					send_push = true
					break
				case "Pagado":
					push_notification = "Gracias por preferirnos."
					send_push = true
					break
			}

			if(send_push){
				$scope.pushNotification(data.order.device, push_notification);
			}

		});
	}

	$scope.changeCSS = function(data){
		if (data == "Solicitada") return "info"
		if (data == "Aprobado" || data =="Despachada") return "success"
		if (data == "Sin Stock" || data == "En proceso") return "warning"
		if (data == "Rechazado") return "danger"
			
	}

	$scope.changeBody = function(data) {
		if (data =="Despachada") return "rgba(70,148,8,0.1)"	
		if (data == "Solicitada") return "rgba(2,154,207,0.1)"
		if (data == "Cuenta") return "rgba(2,154,207,0.1)"
	}

	$scope.pushNotification = function(device, push_notification){
		// Define relevant info
		var privateKey = 'a522d72b9cccf12e2138ad6743be05165f8c977ad7ba5a21';
		//var tokens = ['APA91bHmTZJTAo8s5wuV2dMa34W5ujNT7qxiOha7I_fOVYWXlQBPCu6YG0UvJebSBBtaAjCafC9cwqafGONuchSC2-nqCMiXv5vKy4OslBGjYZGkLVDa9LLXGK0ZozfOVobYqRtwLCut','f3a0e29ba1deb3e80656326af8870b027efe0e3a1aef57c0c05d77420ff7428c'];
		var tokens = [device];
		
		var appId = 'fc7f194b';

		// Encode your key
		var auth = btoa(privateKey + ':');

		// Build the request object
		var req = {
		  method: 'POST',
		  url: 'https://push.ionic.io/api/v1/push',
		  headers: {
		    'Content-Type': 'application/json',
		    'X-Ionic-Application-Id': appId,
		    'Authorization': 'basic ' + auth
		  },
		  data: {
		    "tokens": tokens,
		    "notification": {
		      "alert": push_notification
		    }
		  }
		};

		// Make the API call
		$http(req).success(function(resp){
		  // Handle success
		  console.log("Ionic Push: Push success!");
		}).error(function(error){
		  // Handle error 
		  console.log("Ionic Push: Push error...");
		});
	}
});
