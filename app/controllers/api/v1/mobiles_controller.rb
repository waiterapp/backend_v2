module Api
  module V1
    class MobilesController < ApplicationController
      skip_before_filter :verify_authenticity_token  
      before_filter :set_headers

      respond_to :json

      #Se envian productos y categorias del restaurant
      def index
        @table = Table.find(params[:id])
        if @table
          restaurant_id = @table.restaurant_id
          respond_with @table.to_json(:methods => [:restaurant,:categories,:products])
        else
          render :json => {:result => "NACK"}
        end
      end

      def request_account

        orders = Order.where(:table_id => params[:table_id], :status => "Despachada", :device => params[:device])

        if orders.empty?
          render :json => {:result => "NACK", :message => "No existen ordenes despachadas asociadas a usted."}
        else
          orders.each_with_index do |order, index|
            order.status = "Cuenta"
            order.payment_method = params[:payment_method]
            order.save
          end
          render :json => {:result => "ACK", :message => "Cuenta(s) solicitada(s)."}
        end

        # order = Order.where(:id => params[:order_id], :status => "Despachada")

        # if order.empty?
        #   render :json => {:result => "NACK", :message => "Orden debe estar despachada."}
        # else
        #   order = Order.find(order[0].id)
        #   order.status = "Cuenta"
        #   order.payment_method = params[:payment_method]
        #   if order.save
        #     render :json => {:result => "ACK", :message => "Cuenta solicitada."}
        #   else 
        #     render :json => {:result => "NACK", :message => "Ocurrió un error. Inténtelo nuevamente."}
        #   end
        # end
      end

      #Creacion de nueva orden
      def new_order
        order = Order.create(order_params)
        
        if order.save

          products = JSON.parse(params[:orders_product])
          total = 0

          table = Table.find(order_params[:table_id])

          products.each_with_index do |product, index|
            product[:order_id] = order.id
            product[:status] = "Solicitada"
            
            prod = Product.find(product['product_id'])
            total = total + prod.price*product['qty']

            orders_product = OrdersProduct.create(product)
            orders_product.save
          end
          order.restaurant_id = table.restaurant_id
          order.total = total
          if order.save
            render :json => {:order_id => order.id,:total => total, :status => "Solicitada"}
          else
            render :json => {:order_id => null, :status => "NACK"}
          end
        else
          render :json => {:order_id => null, :status => "NACK"}
        end
      end

      private

      def set_headers
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Expose-Headers'] = 'ETag'
        headers['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS, HEAD'
        headers['Access-Control-Allow-Headers'] = '*,x-requested-with,Content-Type,If-Modified-Since,If-None-Match'
        headers['Access-Control-Max-Age'] = '86400'
      end

      def order_params
        params.require(:order).permit(:restaurant_id, :table_id, :user_id, :status, :device)
      end


    end
  end
end