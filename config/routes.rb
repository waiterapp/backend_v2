require 'api_constraints'
Rails.application.routes.draw do

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      get '/mobiles/:id', to: 'mobiles#index'
      post '/mobiles/new_order', to: 'mobiles#new_order'
      post '/mobiles/request_account', to: 'mobiles#request_account'
      
      get '/restaurants/index/:id', to: 'restaurants#index'
      get '/restaurants/orders/:table_id', to: 'restaurants#orders'
      post '/restaurants/update_product', to: 'restaurants#update_product'
      post '/restaurants/update_order', to: 'restaurants#update_order'
    end
  end

  resources :restaurants, :tables, :products, :categories

  get 'tables/new/:restaurant_id', to: "tables#new"
  get 'tables/index/:restaurant_id', to: "tables#index"

  # You can have the root of your site routed with "root"
  get '/', to: "orders#index2"
  
  post 'restaurants/add_c', to: "restaurants#add_categories"
  post 'restaurants/add_p', to: "restaurants#add_products"




  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
