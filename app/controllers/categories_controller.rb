class CategoriesController < ApplicationController
	
	def create
		category = Category.create(category_params)
		@restaurant = Restaurant.find(params[:restaurant_id])
  	#redirect_to restaurant_path(@restaurant)
  	respond_to do |format|
	    if category.save
        #redirect_to @restaurant
        flash[:success] = 'Categoria guardada.'
        format.html { redirect_to @restaurant }
      else
        render 'new'
      end
    end
	end

  def destroy
    category = Category.find(params[:id])
    category.destroy
    flash[:success] = 'Categoría eliminada.'
    redirect_to :back
  end

	private
  def category_params
    params.require(:category).permit(:name, :image, :order, :restaurant_id)
  end
end
