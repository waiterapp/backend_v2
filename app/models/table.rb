class Table < ActiveRecord::Base
	belongs_to :restaurant
	has_many :orders

	def restaurant
		Restaurant.find(self.restaurant_id)
	end

	def categories
		categories = Category.where(restaurant_id: self.restaurant_id).order(:order)
		
		categories.each_with_index do |obj ,index|

			categories[index].image_file_name =  ApplicationController.helpers.asset_url(obj.image.url(:large))

		end

		categories
	end

	def products
		
		products =  Product.where(restaurant_id: self.restaurant_id).order(:order)
		products.each_with_index do |obj, index|
			products[index].image_file_name =  ApplicationController.helpers.asset_url(obj.image.url(:large))
		end 
		products
	end

	def orders
		orders = Order.where(:table_id => self.id)
		orders = orders.where("status!= 'Pagado'")
		orders
	end 
end
