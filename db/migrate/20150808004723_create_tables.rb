class CreateTables < ActiveRecord::Migration
  def change
    create_table :tables do |t|
      t.string :qr
      t.string :name
      t.integer :restaurant_id
      t.integer :capacity

      t.timestamps null: false
    end
  end
end
