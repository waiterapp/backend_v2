class AddStatusToOrdersProducts < ActiveRecord::Migration
  def change
    add_column :orders_products, :status, :string
  end
end
