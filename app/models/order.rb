class Order < ActiveRecord::Base
	belongs_to :table
  belongs_to :restaurant

	has_many :orders_products
	has_many :products, :through => :orders_products

	#after_create {|order| order.message 'create' }
  after_update {|order| order.message 'update' }


  def message action
    msg = { resource: 'orders',
            action: action,
            id: self.id,
            mesa: self.table_id,
            obj: self }

    $redis.publish 'rt-change', msg.to_json
  end

end
