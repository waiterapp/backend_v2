# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Restaurant.create([{name: 'Jonnhy Rockets'}, {name: 'McDonalds'}])

Category.create([{name: 'Entrada', order: 2, restaurant_id: 1},
				{name: 'Hamburguesas', order: 3, restaurant_id: 1},
				{name: 'Babidas', order: 1, restaurant_id: 1}])

Category.create([{name: 'Hamburguesas', order: 2, restaurant_id: 2},
				{name: 'Babidas', order: 1, restaurant_id: 2}])

Product.create([
				{sku: '1',name: 'Ensadala Cesar', description: 'descripcion', restaurant_id: 1, category_id: 1, price: 2500, order:3, qty: 1, unit: 'persona', aprox:1},
				{sku: '2',name: 'Route 66', description: 'descripcion', restaurant_id: 1, category_id: 2, price: 5600, order: 1, qty: 1, unit: 'persona', aprox:1},
				{sku: '3',name: 'Super Cheddar', description: 'descripcion', restaurant_id: 1, category_id: 2, price: 7000, order: 2, qty: 1, unit: 'persona', aprox:1},
				{sku: '4',name: 'Coca Cola', description: 'descripcion', restaurant_id: 1, category_id: 3, price: 900, order: 1, qty: 1, unit: 'persona', aprox:1},
				{sku: '5',name: 'Sprite', description: 'descripcion', restaurant_id: 1, category_id: 3, price: 950, order:2, qty: 1, unit: 'persona', aprox:1}
			])

Product.create([
				{sku: '6',name: 'Doble cuarto libra', description: 'descripcion', restaurant_id: 2, category_id: 4, price: 3500, order:1},
				{sku: '7',name: 'Big Mac', description: 'descripcion', restaurant_id: 2, category_id: 4, price: 3800, order: 2},
				{sku: '8',name: 'Coca Cola Zero', description: 'descripcion', restaurant_id: 2, category_id: 5, price: 800, order: 2},
				{sku: '9',name: 'Sprite Zero', description: 'descripcion', restaurant_id: 2, category_id: 5, price: 750, order: 1}
			])

Table.create([
			{qr: 1, name: 'Mesa 1' ,restaurant_id:1, capacity:4},
			{qr: 2, name: 'Mesa 2' ,restaurant_id:1, capacity:6},
			{qr: 3, name: 'Mesa 3' ,restaurant_id:1, capacity:8},
			{qr: 4, name: 'Mesa 1' ,restaurant_id:2, capacity:4},
			{qr: 5, name: 'Mesa 2' ,restaurant_id:2, capacity:6},
			{qr: 6, name: 'Mesa 3' ,restaurant_id:2, capacity:8}
	])

Order.create([
		{restaurant_id: 1, total: 100, status: 'Solicitada', user_id: 1, table_id: 2, payment_method: ""},
		{restaurant_id: 1, total: 200, status: 'Solicitada', user_id: 2, table_id: 2, payment_method: ""},
		{restaurant_id: 2, total: 100, status: 'Solicitada', user_id: 1, table_id: 6, payment_method: ""}
	])

OrdersProduct.create([
		{ order_id: 1, product_id: 1, qty: 1, status: 'Solicitada', obs: 'Sin crutones' },
		{ order_id: 1, product_id: 4, qty: 1, status: 'Solicitada', obs: ''},
		{ order_id: 2, product_id: 1, qty: 2, status: 'Solicitada', obs: 'Con crutones' },
		{ order_id: 2, product_id: 4, qty: 3, status: 'Solicitada', obs: 'Bien heladas'},
		{ order_id: 3, product_id: 6, qty: 2, status: 'Solicitada', obs: 'Sin queso' },
		{ order_id: 3, product_id: 9, qty: 2, status: 'Solicitada', obs: '' }
	])





