class RemoveStatusIdFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :status_id, :integer
  end
end
