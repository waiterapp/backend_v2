class Restaurant < ActiveRecord::Base
	has_many :categories
	has_many :products
	has_many :tables
	has_many :menus
	has_many :orders

	def self.search(search, page)
		#joins(:type).order('restaurants.name').where('restaurants.name LIKE ?', "%#{search}%").paginate(page: page, per_page: 5)
		order('restaurants.name').where('restaurants.name LIKE ?', "%#{search}%").paginate(page: page, per_page: 5)
		#order('name').paginate(page: page, per_page: 5)
	end

	def tables
		Table.where(:restaurant_id => self.id)
	end


end
